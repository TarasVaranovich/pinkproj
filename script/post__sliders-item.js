var pathString = window.location.toString();
var resultCode = pathString.indexOf("photo.html");

if(resultCode!=-1) {

  var rangeBar = document.getElementById("range-bar");
  var minValue = 0;
  var maxValue = rangeBar.offsetWidth;

  var prevWidth = 0;
  var kScale = 0.5;

  var activeButton = 0;
//crop
  var cropToggle = document.getElementById("crop-range");
  var fillToggle = document.getElementById("fill-range");
  var adjustToggle = document.getElementById("adjust-range");
  var X1 = 0;
  var X2 = 0;
  var clickActive = false;
  window.onload = PreviousElem();
  window.onresize = ResizeRange();
  window.onresize = ResizeRange;

  function ResizeRange() {
    maxValue = rangeBar.offsetWidth;

    kScale = rangeBar.offsetWidth/prevWidth;

    if(rangeBar.offsetWidth!=prevWidth) {
      var adjustLeft = getComputedStyle(adjustToggle);
      var fillLeft = getComputedStyle(fillToggle);
      var cropLeft = getComputedStyle(cropToggle);
      adjustOffsetValue = adjustToggle.style.left = parseInt(adjustLeft.left)*kScale;
      fillOffsetValue = fillToggle.style.left = parseInt(fillLeft.left)*kScale;
      cropOffsetValue = cropToggle.style.left = parseInt(cropLeft.left)*kScale;

      prevWidth = rangeBar.offsetWidth;
    }

  }

  function PreviousElem() {
    prevWidth = rangeBar.offsetWidth;
    adjustOffsetValue = adjustToggle.style.left;
    fillOffsetValue = fillToggle.style.left;
    cropOffsetValue = cropToggle.style.left;
  }

  cropToggle.addEventListener("mousedown", function(event) {
    X1 = cropToggle.offsetLeft;
    X1 = event.pageX - X1;
    clickActive = true;
  });

  cropToggle.addEventListener("mousemove", function(event) {

    if(clickActive) {
      X2 = event.pageX;
      if((minValue<(X2-X1)) && ((X2-X1)<maxValue)) {
        cropOffsetValue = cropToggle.style.left = X2 - X1 + "px";
      }
    }

  });

  cropToggle.addEventListener("mouseup", function(event) {
    clickActive = false;
  });

  cropToggle.addEventListener("mouseout", function(event) {
    clickActive = false;
  });

  //fill


  fillToggle.addEventListener("mousedown", function(event) {
    X1 = fillToggle.offsetLeft;
    X1 = event.pageX - X1;
    clickActive = true;
  });

  fillToggle.addEventListener("mousemove", function(event) {

    if(clickActive) {
      X2 = event.pageX;

      if((minValue<(X2-X1)) && ((X2-X1)<maxValue)) {
        fillOffsetValue = fillToggle.style.left = X2 - X1 + "px";
      }
    }
  });

  fillToggle.addEventListener("mouseup", function(event) {
    clickActive = false;
  });

  fillToggle.addEventListener("mouseout", function(event) {
    clickActive = false;
  });

    //adjust


  adjustToggle.addEventListener("mousedown", function(event) {
    X1 = adjustToggle.offsetLeft;
    X1 = event.pageX - X1;
    clickActive = true;
  });

  adjustToggle.addEventListener("mousemove", function(event) {
    if(clickActive) {
      X2 = event.pageX;

      if((minValue<(X2-X1)) && ((X2-X1)<maxValue)) {
        adjustOffsetValue = adjustToggle.style.left = X2 - X1 + "px";
      }
    }
  });

  adjustToggle.addEventListener("mouseup", function(event) {
    var cropToggleMobile = document.querySelector(".mobile");
    var cropLeft = getComputedStyle(cropToggleMobile);
    if(cropLeft.display.localeCompare("none")==0) {

      for(var i=0; i<buttons.length; i++) {

        if(buttons[i].classList.contains("active")) {
          activeButton = i;
        }
      }
      switch (activeButton) {
        case 0:
        cropOffsetValue = cropToggle.style.left = adjustToggle.style.left;
          break;
        case 1:
        fillOffsetValue = fillToggle.style.left = adjustToggle.style.left;
          break;
        case 2:
        adjustOffsetValue = adjustToggle.style.left;
          break;
        default:
        alert("Element out of range!");
      }
    }
    clickActive = false;
  });

  adjustToggle.addEventListener("mouseout", function(event) {
    var cropToggleMobile = document.querySelector(".mobile");
    var cropLeft = getComputedStyle(cropToggleMobile);

    if(cropLeft.display.localeCompare("none")==0) {

      for(var i=0; i<buttons.length; i++) {
        
        if(buttons[i].classList.contains("active")) {
          activeButton = i;
        }
      }
      switch (activeButton) {
        case 0:
        cropOffsetValue = cropToggle.style.left = adjustToggle.style.left;
          break;
        case 1:
        fillOffsetValue = fillToggle.style.left = adjustToggle.style.left;
          break;
        case 2:
        adjustOffsetValue = adjustToggle.style.left;
          break;
        default:
        alert("Element out of range!");
      }
    }
    clickActive = false;
  });

}
