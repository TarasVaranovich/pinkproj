var pathString = window.location.toString();
var resultCode = pathString.indexOf("index.html");
var resultArr = pathString.split('/');

if(resultCode!=-1 || (resultArr[3]=="")) {

  var uluru = {lat: 59.938699, lng: 30.323068};
  var map = new google.maps.Map(document.getElementById('my_map'), {
    zoom: 18,
    center: uluru,
    mapTypeId: 'roadmap',
    disableDefaultUI: true,
    heading: 90
  });
  
  var marker = new google.maps.Marker({
	    position: uluru,
	    map: map,
     icon: {
		      url: "img/map-marker.png",
		      scaledSize: new google.maps.Size(36, 36)
	     }
  });

}
