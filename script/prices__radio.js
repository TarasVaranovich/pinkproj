var pathString = window.location.toString();
var resultCode = pathString.indexOf("index.html");
var resultArr = pathString.split('/');

if(resultCode!=-1 || (resultArr[3]=="")) {

var firstPriceRadio = document.getElementById("firstVersion");
var secondPriceRadio = document.getElementById("secondVersion");
var thirdPriceRadio = document.getElementById("thirdVersion");

//ID'S:
var baseHeadContent = document.getElementById("baseHead");
var standartHeadContent = document.getElementById("standartHead");
var unlimHeadContent = document.getElementById("unlimHead");
var baseFirstContent = document.getElementById("baseFirst");
var standartFirstContent = document.getElementById("standartFirst");
var unlimFirstContent = document.getElementById("unlimFirst");
var baseSecondContent = document.getElementById("baseSecond");
var standartSecondContent = document.getElementById("standartSecond");
var unlimSecondContent = document.getElementById("unlimSecond");
var baseThirdContent = document.getElementById("baseThird");
var standartThirdContent = document.getElementById("standartThird");
var unlimThirdContent = document.getElementById("unlimThird");

var pricesArray = [[baseHeadContent.innerHTML, baseFirstContent.innerHTML, baseSecondContent.innerHTML, baseThirdContent.innerHTML],
                   [standartHeadContent.innerHTML, standartFirstContent.innerHTML, standartSecondContent.innerHTML, standartThirdContent.innerHTML],
                   [unlimHeadContent.innerHTML, unlimFirstContent.innerHTML, unlimSecondContent.innerHTML, unlimThirdContent.innerHTML]];

firstPriceRadio.disabled = false;
secondPriceRadio.disabled = false;
thirdPriceRadio.disabled = false;

firstPriceRadio.addEventListener("click", function(event) {
    baseHeadContent.innerHTML = pricesArray[2][0];
    baseFirstContent.innerHTML = pricesArray[2][1];
    baseSecondContent.innerHTML = pricesArray[2][2];
    baseThirdContent.innerHTML = pricesArray[2][3];

    standartHeadContent.innerHTML = pricesArray[0][0];
    standartFirstContent.innerHTMLL = pricesArray[0][1];
    standartSecondContent.innerHTML = pricesArray[0][2];
    standartThirdContent.innerHTML = pricesArray[0][3];

    unlimHeadContent.innerHTML = pricesArray[1][0];
    unlimFirstContent.innerHTML = pricesArray[1][1];
    unlimSecondContent.innerHTML = pricesArray[1][2];
    unlimThirdContent.innerHTML = pricesArray[1][3];
  });

secondPriceRadio.addEventListener("click", function(event) {
    baseHeadContent.innerHTML = pricesArray[0][0];
    baseFirstContent.innerHTML = pricesArray[0][1];
    baseSecondContent.innerHTML = pricesArray[0][2];
    baseThirdContent.innerHTML = pricesArray[0][3];

    standartHeadContent.innerHTML = pricesArray[1][0];
    standartFirstContent.innerHTMLL = pricesArray[1][1];
    standartSecondContent.innerHTML = pricesArray[1][2];
    standartThirdContent.innerHTML = pricesArray[1][3];

    unlimHeadContent.innerHTML = pricesArray[2][0];
    unlimFirstContent.innerHTML = pricesArray[2][1];
    unlimSecondContent.innerHTML = pricesArray[2][2];
    unlimThirdContent.innerHTML = pricesArray[2][3];
  });

thirdPriceRadio.addEventListener("click", function(event) {
    baseHeadContent.innerHTML = pricesArray[0][0];
    baseFirstContent.innerHTML = pricesArray[0][1];
    baseSecondContent.innerHTML = pricesArray[0][2];
    baseThirdContent.innerHTML = pricesArray[0][3];

    standartHeadContent.innerHTML = pricesArray[2][0];
    standartFirstContent.innerHTMLL = pricesArray[2][1];
    standartSecondContent.innerHTML = pricesArray[2][2];
    standartThirdContent.innerHTML = pricesArray[2][3];

    unlimHeadContent.innerHTML = pricesArray[1][0];
    unlimFirstContent.innerHTML = pricesArray[1][1];
    unlimSecondContent.innerHTML = pricesArray[1][2];
    unlimThirdContent.innerHTML = pricesArray[1][3];
  });
}
