var pathString = window.location.toString();
var resultCode = pathString.indexOf("index.html");
var resultArr = pathString.split('/');

if(resultCode!=-1 || (resultArr[3]=="")) {

window.onload = CheckedRadio();

function CheckedRadio(){
  document.getElementById('secondFrame').checked = true;
  document.getElementById('secondVersion').checked = true;
}

var feedback__data = '{"feedbacks": [{"name": "Иван&nbspИванов","info": "20&nbspлет, студент", "feedback": "Иванов.Это приложение. Контент Иванова, отзыв Иванова,отзыв Иванова,отзыв Иванова,отзыв Иванова!"},{"name": "Николай&nbspПетров", "info": "25&nbspлет, водитель&nbspтрамвая", "feedback": "Это приложение перевернуло мой мир и позволило по-новому взглянуть на привычные серые вещи! А еще я познакомился со своей будущей женой в комментариях к вложенной фотографии!"},{"name": "Маша&nbspРастеряша","info": "10&nbspлет, школьик","feedback": "Маша.Стильно, модно, молодежно! Контент отзыва Маши,Контент отзыва Маши,Контент отзыва Маши,Контент отзыва Маши"}]}'

var feedbackData = JSON.parse(feedback__data);

var firstRadio = document.getElementById("firstFrame");
var secondRadio = document.getElementById("secondFrame");
var thirdRadio = document.getElementById("thirdFrame");

var userName = document.querySelector(".feedback__user_name");
var userInfo = document.querySelector(".feedback__user_info");
var userText = document.querySelector(".feedback__text");

firstRadio.disabled = false;
secondRadio.disabled = false;
thirdRadio.disabled = false;

firstRadio.addEventListener("click", function(event) {
    userName.innerHTML = feedbackData.feedbacks[0].name;
    userInfo.innerHTML = feedbackData.feedbacks[0].info;
    userText.innerHTML = feedbackData.feedbacks[0].feedback;
  });
secondRadio.addEventListener("click", function(event) {
    userName.innerHTML = feedbackData.feedbacks[1].name;
    userInfo.innerHTML = feedbackData.feedbacks[1].info;
    userText.innerHTML = feedbackData.feedbacks[1].feedback;
  });
thirdRadio.addEventListener("click", function(event) {
    userName.innerHTML = feedbackData.feedbacks[2].name;
    userInfo.innerHTML = feedbackData.feedbacks[2].info;
    userText.innerHTML = feedbackData.feedbacks[2].feedback;
  });

var leftButton = document.getElementById("leftFrame");
var rightButton = document.getElementById("rightFrame");

var currentNumber = 1;
leftButton.addEventListener("click", function(event) {

    if(currentNumber > 0) {
        currentNumber = currentNumber - 1;
        userName.innerHTML = feedbackData.feedbacks[currentNumber].name;
        userInfo.innerHTML = feedbackData.feedbacks[currentNumber].info;
        userText.innerHTML = feedbackData.feedbacks[currentNumber].feedback;
    }

  });
rightButton.addEventListener("click", function(event) {

    if(currentNumber < feedbackData.feedbacks.length - 1) {
        currentNumber = currentNumber + 1;
        userName.innerHTML = feedbackData.feedbacks[currentNumber].name;
        userInfo.innerHTML = feedbackData.feedbacks[currentNumber].info;
        userText.innerHTML = feedbackData.feedbacks[currentNumber].feedback;
    }

  });

}
