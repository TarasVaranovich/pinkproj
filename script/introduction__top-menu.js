var modalButton = document.querySelector(".introduction__top-menu_button");

modalButton.addEventListener("click", function(event) {
    event.preventDefault();
    if(!modalButton.classList.contains("introduction__top-menu_button_active")) {
        var menuBar = document.querySelector(".introduction__top-menu");
        var modalForm = document.querySelector(".introduction__top-menu_nav");
        menuBar.classList.add("introduction__top-menu_active");
        modalForm.classList.add("introduction__top-menu_nav_active");
        modalButton.classList.add("introduction__top-menu_button_active");
  } else {
        var modalForm = document.querySelector(".introduction__top-menu_nav_active");
        var menuBar = document.querySelector(".introduction__top-menu_active");
        modalForm.classList.remove("introduction__top-menu_nav_active");
        modalButton.classList.remove("introduction__top-menu_button_active");
        menuBar.classList.remove("introduction__top-menu_active");
  }
  });
  window.addEventListener("keydown", function(event) {
    if (event.keyCode === 27) {
      var modalForm = document.querySelector(".introduction__top-menu_nav_active");
      var menuBar = document.querySelector(".introduction__top-menu_active");

      if (modalForm.classList.contains("introduction__top-menu_nav_active")) {
          modalForm.classList.remove("introduction__top-menu_nav_active");
          modalButton.classList.remove("introduction__top-menu_button_active");
          menuBar.classList.remove("introduction__top-menu_active");
      }
      
    }
  });
