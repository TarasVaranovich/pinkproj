var fillOffsetValue = 0;// this value to be setting in .post__sliders-item OnResize function
var cropOffsetValue = 0;// this value to be setting in .post__sliders-item OnResize function
var adjustOffsetValue = 0;// this value to be setting in .post__sliders-item OnResize function

var pathString = window.location.toString();
var resultCode = pathString.indexOf("photo.html");

if(resultCode!=-1) {

  var buttons = [];
  buttons.push(document.querySelector(".post__sliders_crop"));
  buttons.push(document.querySelector(".post__sliders_fill"));
  buttons.push(document.querySelector(".post__sliders_adjust"));

  for(var i=0; i<buttons.length; i++) {
    buttons[i].addEventListener("click", function(event) {
      ColorChange(this);
      SetOffset(this);
      });
  }
  
  function ColorChange(button) {
    for(var i=0; i<buttons.length; i++) {
      buttons[i].classList.remove("active");
    }
    button.classList.add("active");
  };

  function SetOffset(button) {
    var adjustToggleSwitch = document.getElementById("adjust-range");
    if(button.classList.contains("post__sliders_crop")) {

      adjustToggle.style.left = cropOffsetValue;

    } else if (button.classList.contains("post__sliders_fill")) {

      adjustToggle.style.left = fillOffsetValue;

    } else {

      adjustToggle.style.left = adjustOffsetValue;

    }
  }
}
